import json

import uvicorn
from fastapi import FastAPI, WebSocket
from fastapi.responses import HTMLResponse, Response
import cv2
from cvzone.HandTrackingModule import HandDetector
from imutils.video import VideoStream
import paho.mqtt.client as mqtt
import asyncio
import time


unacked_publish = set()
main_time = 0
response = None

def on_connect( client, userdata, flags, reason_code, properties):
    print(f"Connected with result code {reason_code}\n")
    client.subscribe("Topic1")


# Funktion, die bei einem Publish in ein Topic ausgeführt wird.
def on_publish( client, userdata, mid, reason_code, properties):
    try:
        userdata.remove(mid)  # Removed die gesendete Nachricht vom Speicher, um eine doppelte Sendung zu verhindern
    except KeyError:
        "Something went wrong. Please check if you have removed the mid from the userdata"


# Funktion zur Verbindung mit MQTT
def connectwithmqtt( adress: str, targetport: int):
    mqttc = mqtt.Client(mqtt.CallbackAPIVersion.VERSION2, userdata=None)  # Intitalisierung des Clients
    mqttc.on_connect = on_connect
    mqttc.on_publish = on_publish
    mqttc.user_data_set(unacked_publish)  # Den lokalen Speicher als User data setzen
    mqttc.username_pw_set(username="standardUser",
                          password="GreatHHZ4Ever!")  # Username und Passwort - vorgegeben vom MQTT Server
    mqttc.connect(host=adress, port=targetport)
    while not mqttc.is_connected():
        mqttc.loop()
        print("Waiting for connection...")
    return mqttc


# Funktion zum senden einer Nachricht
def sendMessage(mqttc, topic, message):
    mqttc.loop_start()  # Started einen gethreaded connection loop zum senden der Nachrichten.
    msg = mqttc.publish(topic, message, qos=1)
    unacked_publish.add(msg.mid)  # Mid sind die Daten der aktuellen Nachricht
    while len(unacked_publish):
        time.sleep(0.1)  # Wartet bis alle gequeten Nachrichten gepublished werden
    msg.wait_for_publish()


mqttClient = connectwithmqtt(adress="localhost", targetport=1883)

app = FastAPI()

# Video-Capture initialisieren
try:
    video_capture = VideoStream("http://localhost:80/video_feed").start()
except Exception as e:
    print(f"We couldn't reach the camera")
    exit()

# Hand-Detektor initialisieren
detector = HandDetector(maxHands=2)

while True:
    frame = video_capture.read()
    if frame is None:
        print("Sorry, couldn't receive videostream")
        exit()

    # Handgesten-Erkennung
    hands, img = detector.findHands(frame)
    current_time = time.time()  # Aktueller Zeitpunkt
    if current_time - main_time > 5:  # Timer
        main_time = current_time
        if hands:
            # Zähle die Anzahl der gestreckten Finger
            total_fingers = 0
            for hand in hands:
                fingers = detector.fingersUp(hand)
                total_fingers += sum(fingers)
                response = {
                    "fingers": fingers,
                    "total_fingers": total_fingers
                }
            if response is not None:
                sendMessage(mqttc=mqttClient, topic="Topic1", message=json.dumps(response))


    # Bild fürs Debugging anzeigen
    cv2.imshow("Video", img)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        print("Goodbye!")
        mqttClient.disconnect()
        mqttClient.loop_stop()
        break
video_capture.stop()
cv2.destroyAllWindows()

'''
@app.websocket("/ws")
async def websocket_endpoint(websocket: WebSocket):
    await websocket.accept()
    print("WebSocket connection accepted")
    while True:
        frame = video_capture.read()
        if frame is None:
            continue

        # Handgesten-Erkennung
        hands, img = detector.findHands(frame, flipType=False)
        if hands:
            # Zähle die Anzahl der gestreckten Finger
            total_fingers = 0
            for hand in hands:
                fingers = detector.fingersUp(hand)
                total_fingers += sum(fingers)

            # Begrenze die Fingeranzahl auf 7, da wir 7 Spalten haben (0-6)
            selected_column = min(total_fingers, 6)
            print("Selected column:", selected_column)
            try:
                await websocket.send_json({"column": selected_column})
            except Exception as e:
                print(f"Error sending message: {str(e)}")

        # Warte drei Sekunden bevor die nächste Nachricht gesendet wird
        await asyncio.sleep(3)

        # Bild fürs Debugging anzeigen
        cv2.imshow("Video", img)
        cv2.waitKey(1)

# Endpoint für die HTML-Seite
@app.get("/", response_class=HTMLResponse)
async def get_html():
    with open("index.html", "r") as file:
        html_content = file.read()
    return HTMLResponse(content=html_content, status_code=200)

# Endpoint für die CSS-Datei
@app.get("/style.css")
async def get_style():
    with open("style.css", "r") as file:
        css_content = file.read()
    return Response(content=css_content, media_type="text/css")

# Endpoint für die JavaScript-Datei
@app.get("/app.js")
async def get_script():
    with open("app.js", "r") as file:
        script_content = file.read()
    return Response(content=script_content, media_type="application/javascript")

# Endpoint für die Bilddatei
@app.get("/four.jpg")
async def get_image():
    with open("four.jpg", "rb") as file:
        image_content = file.read()
    return Response(content=image_content, media_type="image/jpeg")

if __name__ == "__main__":
    uvicorn.run(app, host="localhost", port=89)
    
    
 # Begrenze die Fingeranzahl auf 7, da wir 7 Spalten haben (0-6)
            selected_column = min(total_fingers, 6)
            print("Selected column:", selected_column)
            try:
                await websocket.send_json({"column": selected_column})
            except Exception as e:
                print(f"Error sending message: {str(e)}")
'''